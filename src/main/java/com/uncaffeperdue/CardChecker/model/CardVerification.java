package com.uncaffeperdue.CardChecker.model;

import java.time.OffsetDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class CardVerification {
	@Id
	@GeneratedValue
	private long id;
	
	private int cardNumber;
	
	@NotNull
	private OffsetDateTime verificationDate;
	
	private String scheme;
	
	private String bank;
	
	private String type;
	
	private boolean success;
}
