package com.uncaffeperdue.CardChecker.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uncaffeperdue.CardChecker.dto.VerificationStatistics;
import com.uncaffeperdue.CardChecker.model.CardVerification;
import com.uncaffeperdue.CardChecker.response.VerificationResponse;
import com.uncaffeperdue.CardChecker.service.CardVerificationService;

@Controller
@RequestMapping(value="/card-scheme")
public class CardVerificationController {
	@Autowired
	private CardVerificationService service;
	
	
	@RequestMapping(value = "/verify/{card}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VerificationResponse getCardVerification(@PathVariable("card") int cardNumber) throws IOException {
		CardVerification cardVerification = service.getCardVerification(cardNumber);
		return VerificationResponse.buildResponse(cardVerification);
	}
	
	@RequestMapping(value = "/stats", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public VerificationStatistics getVerificationStats(@RequestParam("start") int start, @RequestParam("limit") int limit) {
		
		return service.getVerificationStats(start, limit);
	}
}
