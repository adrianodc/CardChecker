package com.uncaffeperdue.CardChecker.repository;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.uncaffeperdue.CardChecker.dto.VerificationCountDto;
import com.uncaffeperdue.CardChecker.dto.VerificationStatistics;
import com.uncaffeperdue.CardChecker.model.CardVerification;

import lombok.AllArgsConstructor;

@Repository
@AllArgsConstructor
public class CardVerificationRepository {
	@PersistenceContext
	private EntityManager entityManager;
	
	private final String QUERY_TO_STATS = "select card_number, count(id)"
			+ " from card_verification"
			+ " group by card_number"
			+ " limit ? offset ?";
	private final String QUERY_TO_COUNT = "select count(card_number)"
			+ " from card_verification"
			+ " group by card_number";
	
	@Transactional
	public void createCardVerification(CardVerification cardVerification) {
		entityManager.persist(cardVerification);
	}
	
	@Transactional
	public VerificationStatistics getVerificationStats(int start, int limit) {
		Query queryStat = entityManager.createNativeQuery(QUERY_TO_STATS);
		queryStat.setParameter(1, limit);
		queryStat.setParameter(2, start);
		List<VerificationCountDto> verifications = new LinkedList<>();
		List<Object[]> resultList = queryStat.getResultList();
		
		Query queryCount = entityManager.createNativeQuery(QUERY_TO_COUNT);
		for (Object[] obj : resultList) {
			VerificationCountDto v = new VerificationCountDto(obj[0].toString(), new Long(obj[1].toString()));
			verifications.add(v);
		}
		return VerificationStatistics.builder()
			.start(start)
			.limit(limit)
			.payload(verifications)
			.size(Long.parseLong(queryCount.getSingleResult().toString()))
			.build();
	}
}
