package com.uncaffeperdue.CardChecker.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.OffsetDateTime;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.uncaffeperdue.CardChecker.dto.VerificationStatistics;
import com.uncaffeperdue.CardChecker.model.CardVerification;
import com.uncaffeperdue.CardChecker.repository.CardVerificationRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class CardVerificationService {
	private final String SERVICE_URL = "https://lookup.binlist.net/";
	private final String SCHEME_KEY = "scheme";
	private final String TYPE_KEY = "type";
	private final String BANK_KEY = "bank";
	private final String BANK_NAME_STRING = "name";
	
	private CardVerificationRepository repository;
	
	public CardVerification getCardVerification(int cardNumber) throws IOException {
		return getFromService(cardNumber);
	}
	
	public VerificationStatistics getVerificationStats(int start, int limit) {
		return repository.getVerificationStats(start, limit);
	}
	
	private CardVerification getFromService(int cardNumber) throws IOException {
		CardVerification cardVerification;  
		URL obj = new URL(SERVICE_URL + cardNumber);
		  HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		  if(con.getResponseCode() != HttpURLConnection.HTTP_OK) {
			  cardVerification = createInvalidVerification(cardNumber);
		  } else {
			  BufferedReader in =new BufferedReader(
			  new InputStreamReader(con.getInputStream()));
			  String inputLine;
			  StringBuffer response = new StringBuffer();
			   while ((inputLine = in.readLine()) != null) {
			     response.append(inputLine);
			   } in .close();
			   JSONObject myResponse = new JSONObject(response.toString());
			   cardVerification = convertFromJSONObject(myResponse, cardNumber);
		  }
		  repository.createCardVerification(cardVerification);
		  return cardVerification;
	}
	
	private CardVerification convertFromJSONObject(JSONObject obj, int cardNumber) {
		CardVerification cardVerification = new CardVerification();
		cardVerification.setScheme(obj.getString(SCHEME_KEY));
		cardVerification.setCardNumber(cardNumber);
		cardVerification.setType(obj.getString(TYPE_KEY));
		JSONObject bank = obj.getJSONObject(BANK_KEY);
		cardVerification.setBank(bank.getString(BANK_NAME_STRING) != null ? bank.getString(BANK_NAME_STRING) : null);
		cardVerification.setSuccess(true);
		cardVerification.setVerificationDate(OffsetDateTime.now());
		return cardVerification;
	}
	
	private CardVerification createInvalidVerification(int cardNumber) {
		CardVerification cardVerification = new CardVerification();
		cardVerification.setCardNumber(cardNumber);
		cardVerification.setSuccess(false);
		cardVerification.setVerificationDate(OffsetDateTime.now());
		return cardVerification;
	}
}
