package com.uncaffeperdue.CardChecker.dto;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VerificationStatistics {
	private long start;
	private long limit;
	private long size;
	
	List<VerificationCountDto> payload;
}
