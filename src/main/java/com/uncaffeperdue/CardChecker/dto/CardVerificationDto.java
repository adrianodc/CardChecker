package com.uncaffeperdue.CardChecker.dto;

import com.uncaffeperdue.CardChecker.model.CardVerification;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CardVerificationDto {
	private String scheme;
	
	private String type;

	private String bank;
	
	public static CardVerificationDto getDtoFrom(CardVerification cardVerification) {
		return CardVerificationDto
				.builder().
				bank(cardVerification.getBank())
				.type(cardVerification.getType())
				.scheme(cardVerification.getScheme())
				.build();
	}
}
