package com.uncaffeperdue.CardChecker.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class VerificationCountDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8559008333047739909L;
	private String number;
	private long verifications;
}
