package com.uncaffeperdue.CardChecker.response;

import com.uncaffeperdue.CardChecker.dto.CardVerificationDto;
import com.uncaffeperdue.CardChecker.model.CardVerification;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class VerificationResponse {
	private boolean success;
	private CardVerificationDto payload;
	
	public static VerificationResponse buildResponse(CardVerification cardVerification) {
		VerificationResponseBuilder builder = builder().success(cardVerification.isSuccess());
		if(cardVerification.isSuccess()) {
			builder = builder.payload(CardVerificationDto.getDtoFrom(cardVerification));
		}
		return builder.build();
	}
}
